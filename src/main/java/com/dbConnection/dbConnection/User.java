package com.dbConnection.dbConnection;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="UserTable") //ważne, nazwa musi byc inna niż nazwa klasy
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//IDENTITY inkrementacja o 1
    private long id;

    private String login;
    private String password;
    private int valueNo;
    //private LocalDateTime lastLogindate;

    public User(String login, String password, int valueNo) {
        this.login = login;
        this.password = password;
        this.valueNo = valueNo;
    }

    //dodane:
    public String toString() {
        return String.format("User[id=%d,login='%s', password='%s', valueNo=%d]", id, login, password, valueNo);
    }


    public User() { //bezparametrowy konstruktor
    }

    public long getId(){
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
