package com.dbConnection.dbConnection;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//nowe:
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DbConnectionApplication {

	////private static final Logger log = LoggerFactory.getLogger(DbConnectionApplication.class);//

	public static void main(String[] args) {
		SpringApplication.run(DbConnectionApplication.class, args);
	}


	//SpringApplication.run...
	////@Bean
	////public CommandLineRunner demo(UserRepo repository) { //dodane
	////	return (args) -> {
	////		repository.save(new User("Piotrek","password123", 123));

	////		log.info("Users found with findAll():");
	////		log.info("----------------------------");
	////		for (User user : repository.findAll()) {
	////			log.info(user.toString());
	////		}
	////		log.info("");
	////	};

	////}

}


// w pliku application.properties :
//spring.jpa.hibernate.ddl-auto=none:
//Zapewni to, że inicjalizacja oparta na skrypcie zostanie wykonana bezpośrednio przy użyciu schema.sql i data.sql.

//spring.jpa.defer-datasource-initialization=true:
//Zapewni to, że po utworzeniu schematu Hibernate, dodatkowo zostanie odczytany plik schema.sql dla wszelkich dodatkowych zmian w schemacie oraz zostanie wykonany data.sql w celu zapełnienia bazy danych.


//inicjalizacja oparta na skrypcie jest wykonywana domyślnie tylko dla wbudowanych baz danych, aby zawsze inicjować bazę danych za pomocą skryptów użyć:
//spring.sql.init.mode=always